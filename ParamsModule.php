<?php

namespace eapanel\app_params_settings;

use Yii;

class ParamsModule extends \yii\base\Module
{
    public $defaultRoute = 'main';
    
    public $controllerNamespace = 'eapanel\app_params_settings\controllers';
    
    public $paramsFilePath = '@app/config/params.php';

    public function init()
    {
        parent::init();
        $this->registerTranslations();

        // custom initialization code goes here
    }
    
    protected function registerTranslations()
    {
        Yii::$app->i18n->translations['yii2_app_params_settings/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en-US',
            'basePath' => '@eapanel/app_params_settings/messages',
            'fileMap' => [
                'yii2_app_params_settings/system' => 'system.php',
                'yii2_app_params_settings/app' => 'app.php'
            ],
        ];
    }
    
    public static function t($category, $message, $params = [], $language = null)
    {
        return Yii::t('yii2_app_params_settings/' . $category, $message, $params, $language);
    }
}
