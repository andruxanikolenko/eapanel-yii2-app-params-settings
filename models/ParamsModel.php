<?php
/**
 * Файл класса модели представляющей файл конфигурации приложения
 */

namespace eapanel\app_params_settings\models;

use eapanel\app_params_settings\ParamsModule as Module;
use yii\helpers\VarDumper;

/**
 * Представление данных из файла конфигурации приложения.
 *
 * @property-read string $paramsFilePath read-only виртуальный аттрибут {@see ParamsModel::getParamsFilePath()}  
 * представляет абсолютный путь к файлу конфигурации приложения
 * @author Tartharia
 */
class ParamsModel extends \yii\base\Model{
    
    private $_attributes=[];
    /**
     * Псевдоним пути к файлу конфигурации приложения.
     * @var string
     */
    public $paramsFilePathAlias;
    
    public function init() {
        if(!isset($this->paramsFilePath))
        {
            throw new \yii\base\InvalidConfigException(Module::t('system','$paramsFilePathAlias variable must be set when creating the object'));
        }
        $this->loadFromFile();        
    }
    
    public function getAttributes($names = null, $except = array()) {
        
    }
    
    public function attributes() {
        $attributes = [];
        foreach ($this->_attributes as $name => $value) {
            if(is_string($value))
            {
                $attributes[]=$name;
            }
        }
        return $attributes;
    }

    public function getParamsFilePath()
    {
        return \Yii::getAlias($this->paramsFilePathAlias);
    }
    
    public function save()
    {
        $this->saveToFile();
    }

    protected function loadFromFile()
    {
        $this->_attributes = require $this->paramsFilePath;
    }
    
    protected function saveToFile()
    {
        file_put_contents($this->paramsFilePath,  "<?php\nreturn " . VarDumper::export($this->_attributes). ";\n",LOCK_EX);
    }
    
    public function __get($name) {
        if(key_exists($name, $this->_attributes))
        {
            $result = $this->_attributes[$name];
        }
            else{
                $result=parent::__get($name);
            }
        return $result;
    }
    
    public function setAttributes($values, $safeOnly = false) {
        if (is_array($values)) {
            $attributes = array_flip($safeOnly ? $this->safeAttributes() : $this->attributes());
            foreach ($values as $name => $value) {
                if (isset($attributes[$name])) {
                    $this->_attributes[$name] = $value;
                } elseif ($safeOnly) {
                    $this->onUnsafeAttribute($name, $value);
                }
            }
        }
    }
}
