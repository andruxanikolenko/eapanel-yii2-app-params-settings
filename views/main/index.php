<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use eapanel\app_params_settings\ParamsModule as Module;

/* @var $model eapanel\app_params_settings\models\ParamsModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="app_params_settings-main-index">
    <h1><?= $model->paramsFilePath ?></h1>
    <?php $form = ActiveForm::begin();
        foreach ($model->attributes() as $attribute):
            echo $form->field($model,$attribute);
        endforeach;
        echo Html::submitButton(Module::t('app','Save settings'),['class'=>'btn btn-success']);
    ?>
    <?php ActiveForm::end()?>
</div>
