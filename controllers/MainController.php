<?php

namespace eapanel\app_params_settings\controllers;

use yii\web\Controller;
use yii\filters\AccessControl;
use eapanel\app_params_settings\models\ParamsModel;

class MainController extends Controller
{
    public function behaviors() {
        return[
            'access'=>[
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'=>true,
                        'roles' => ['Administrator'],
                    ]
                ]
            ]
        ];
    }

    public function actionIndex()
    {
        $model = new ParamsModel(['paramsFilePathAlias' =>  $this->module->paramsFilePath]);
        if($model->load(\Yii::$app->request->post()))
        {
            $model->save();
        }
        return $this->render('index',['model'=>$model]);
    }
}
